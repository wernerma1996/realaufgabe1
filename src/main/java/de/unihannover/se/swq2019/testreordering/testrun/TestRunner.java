package de.unihannover.se.swq2019.testreordering.testrun;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.bpelunit.framework.base.BPELUnitBaseRunner;
import net.bpelunit.framework.control.deploy.simple.FixedDeployer;
import net.bpelunit.framework.control.ext.IBPELDeployer;
import net.bpelunit.framework.control.ext.ISOAPEncoder;
import net.bpelunit.framework.control.result.XMLResultProducer;
import net.bpelunit.framework.control.soap.DocumentLiteralEncoder;
import net.bpelunit.framework.exception.ConfigurationException;
import net.bpelunit.framework.exception.DeploymentException;
import net.bpelunit.framework.exception.SpecificationException;
import net.bpelunit.framework.exception.TestCaseNotFoundException;
import net.bpelunit.framework.model.test.ITestResultListener;
import net.bpelunit.framework.model.test.TestSuite;
import net.bpelunit.framework.xml.result.XMLTestResultDocument;

public class TestRunner extends BPELUnitBaseRunner {

	private TestSuite suite;

	public TestRunner() throws ConfigurationException {
		initialize(new HashMap<>());
	}

	public TestSuite loadTestSuite(File testSuite) throws SpecificationException {
		this.suite = super.loadTestSuite(testSuite);
		
		return this.suite;
	}
	
	public XMLTestResultDocument run(ITestResultListener listener, List<String> testCaseNames)
			throws SpecificationException, DeploymentException, ConfigurationException, TestCaseNotFoundException {

		if(testCaseNames != null && testCaseNames.size() > 0) {
			suite.setFilter(testCaseNames);
		}
		
		if (listener != null) {
			suite.addResultListener(listener);
		}

		try {
			suite.setUp();
		} catch (DeploymentException e) {
			try {
				suite.shutDown();
			} catch (DeploymentException e2) {
			}
			throw e;
		}

		try {
			suite.run();
			if (listener != null) {
				suite.removeResultListener(listener);
			}

		} finally {
			suite.shutDown();
		}
		return XMLResultProducer.getXMLResults(suite);
	}
	
	@Override
	public IBPELDeployer createNewDeployer(String type) throws SpecificationException {
		return new FixedDeployer();
	}
	
	@Override
	public void configureDeployers() throws ConfigurationException {
	}
	
	@Override
	public Map<String, String> getGlobalConfigurationForDeployer(IBPELDeployer deployer) {
		return Collections.emptyMap();
	}
	
	@Override
	public void configureExtensions() throws ConfigurationException {
	}
	
	@Override
	public void configureLogging() throws ConfigurationException {
	}
	
	@Override
	public ISOAPEncoder createNewSOAPEncoder(String styleEncoding) throws SpecificationException {
		return new DocumentLiteralEncoder();
	}
}
