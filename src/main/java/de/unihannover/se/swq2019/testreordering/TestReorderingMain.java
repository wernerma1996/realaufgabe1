package de.unihannover.se.swq2019.testreordering;

import java.io.File;

import javax.swing.UIManager;

import controller.Controller;
import model.Model;
import view.View;

/*
 * Program structure: ModelViewController
 * GUI built with WindowBuilder extension for Eclipse
 */
public class TestReorderingMain {

	public static void main(String[] args) throws Exception {
		final File fileToOpen = args.length == 1 ? new File(args[0]) : null;

		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		
		View v = new View();
		Model m = new Model();
		Controller c = new Controller(m, v);
		
		c.registerEvents();
		if (fileToOpen != null) {
			c.initTestFile(fileToOpen);
		}
		
		v.setVisible(true);
	}
}
