package model;

import java.util.ArrayList;
import java.util.List;

public class TestCaseManager {
	private List<String> notTested;
	private List<String> failed;
	private List<String> successful;
	
	public TestCaseManager(List<String> testCases) {
		this.notTested = new ArrayList<String>();
		this.failed = new ArrayList<String>();
		this.successful = new ArrayList<String>();
		notTested.addAll(testCases);
	}
	
	public List<String> getOrderedTests() {
		List<String> orderedTests = new ArrayList<String>();
		orderedTests.addAll(notTested);
		orderedTests.addAll(failed);
		orderedTests.addAll(successful);
		return orderedTests;
	}
	
	public void updateTest(String testName, boolean passed) {
		notTested.remove(testName);
		failed.remove(testName);
		successful.remove(testName);
		if(passed) {
			successful.add(testName);
		}
		else {
			failed.add(testName);
		}
	}
	
	public List<String> getFailedTests() {
		List<String> failedTests = new ArrayList<String>();
		if (failed.isEmpty()) {
			failedTests.add("none");
		}
		else {
			failedTests.addAll(failed);
		}
		return failedTests;
	}
	
	public SuiteStatus getSuiteStatus() {
		if (failed.isEmpty()) {
			return SuiteStatus.SUCCESS;
		}
		else {
			return SuiteStatus.FAILED;
		}
	}
}
