package model;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.unihannover.se.swq2019.testreordering.testrun.TestRunner;
import log.MyLog;
import net.bpelunit.framework.exception.ConfigurationException;
import net.bpelunit.framework.exception.DeploymentException;
import net.bpelunit.framework.exception.SpecificationException;
import net.bpelunit.framework.exception.TestCaseNotFoundException;
import net.bpelunit.framework.model.test.ITestResultListener;
import net.bpelunit.framework.model.test.TestCase;
import net.bpelunit.framework.model.test.TestSuite;
import net.bpelunit.framework.model.test.report.ITestArtefact;
import net.bpelunit.framework.xml.result.XMLTestResultDocument;

public class Model implements ITestResultListener{
	private TestSuite testSuite;
	private TestRunner testRunner;
	private SuiteStatus suiteStatus;

	private String testSuiteName;
	
	private TestCaseManager testCaseManager;
	
	private File xmlOutDirectory;
	private XMLTestResultDocument xmlReport;
		
	public Model() {
		try {
			testRunner = new TestRunner();
		} catch (ConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		suiteStatus = SuiteStatus.NOT_EXECUTED;
	}
	
	public void openTestSuite(File testSuiteFile) throws SpecificationException {
		this.testSuite = testRunner.loadTestSuite(testSuiteFile);
		suiteStatus = SuiteStatus.NOT_EXECUTED;
		testSuiteName = testSuiteFile.getName();
		testCaseManager = new TestCaseManager(testSuite.getTestCases());
	}
	
	public void executeTestSuite() throws SpecificationException, DeploymentException, ConfigurationException, TestCaseNotFoundException {
		suiteStatus = SuiteStatus.EXECUTING;
		MyLog.log("==========================================");
		MyLog.log("Running Test Suite " + testSuiteName);

		xmlReport = testRunner.run(this, testCaseManager.getOrderedTests());
		
		MyLog.log("Failed Test Summary:");
		for (String failed : testCaseManager.getFailedTests()) {
			MyLog.log("    " + failed);
		}
		
		suiteStatus = testCaseManager.getSuiteStatus();
		
		MyLog.log("Test Suite finished");
		MyLog.log("==========================================");
	}

	@Override
	public void testCaseStarted(TestCase testCase) {
		MyLog.log("    Started Test Case " + testCase);
	}

	@Override
	public void testCaseEnded(TestCase testCase) {
		testCaseManager.updateTest(testCase.getName(), testCase.getStatus().isPassed());
		MyLog.log("    Finished Test Case " + testCase + " (" + testCase.getStatus() + ")");
	}

	@Override
	public void progress(ITestArtefact testArtefact) {
		// TODO Auto-generated method stub
		
	}

	public SuiteStatus getSuiteStatus() {
		return suiteStatus;
	}

	public void setXmlOutDirectory(File xmlOutDirectory) {
		this.xmlOutDirectory = xmlOutDirectory;
	}
	
	public void outputXmlReport() throws IOException {
		if (xmlOutDirectory == null) {
			// no report output directory was selected.
			// this would result in an IOException when calling xmlReport.save,
			// which is not the expected behavior.
			throw new NullPointerException();
		}
		
		xmlOutDirectory.mkdir();
		
		String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss'.txt'").format(new Date());
		String outFileName = xmlOutDirectory + File.separator + "report_" + timeStamp + ".xml";
		File outFile = new File(outFileName);

		outFile.createNewFile();
		xmlReport.save(outFile);
		MyLog.log("Log written to " + outFile.toString());
	}
}
