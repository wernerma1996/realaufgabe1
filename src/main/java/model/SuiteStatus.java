package model;

public enum SuiteStatus {
	NOT_EXECUTED,
	EXECUTING,
	FAILED,
	SUCCESS
}
