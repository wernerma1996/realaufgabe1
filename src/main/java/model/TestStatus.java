package model;

public enum TestStatus {
	NOT_TESTED,
	FAILED,
	SUCCESSFUL
}
