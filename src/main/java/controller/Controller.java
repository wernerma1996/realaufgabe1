package controller;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import log.MyLog;
import model.Model;
import model.SuiteStatus;
import net.bpelunit.framework.exception.ConfigurationException;
import net.bpelunit.framework.exception.DeploymentException;
import net.bpelunit.framework.exception.SpecificationException;
import net.bpelunit.framework.exception.TestCaseNotFoundException;
import view.View;

public class Controller implements ActionListener {
	private Model model;
	private View view;
	private File testSuiteFile;
	private JFileChooser fileChooserSuite;
	private JFileChooser fileChooserLogDir;

	public Controller(Model m, View v) {
		this.view = v;
		this.model = m;
		fileChooserSuite = new JFileChooser(System.getProperty("user.dir"));
		fileChooserSuite.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fileChooserLogDir = new JFileChooser(System.getProperty("user.dir"));
		fileChooserLogDir.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fileChooserLogDir.setAcceptAllFileFilterUsed(false);
	}

	public void registerEvents() {
		view.getBtnLoadTestcase().addActionListener(this);
		view.getBtnRun().addActionListener(this);
		view.getBtnSelectLogDirectory().addActionListener(this);
		MyLog.init(view.getTextArea());
		updateStatusOutput(model.getSuiteStatus());
		model.setXmlOutDirectory(new File(System.getProperty("user.dir") + File.separator + "log"));
	}
	
	public void initTestFile(File testSuiteFile) {
		this.testSuiteFile = testSuiteFile;
		try {
			model.openTestSuite(testSuiteFile);
			view.getTxtCurrentTestcase().setText(testSuiteFile.getName());
			view.getBtnRun().setEnabled(true);
			MyLog.log("Loading Test Suite File successful!");
		} catch (SpecificationException e) {
			MyLog.log("Loading Test Suite File failed!");
			view.getTxtCurrentTestcase().setText("none");
			JOptionPane.showMessageDialog(view, "Loading Test Suite File failed!", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Object src = e.getSource();
		if (src == view.getBtnLoadTestcase()) {
			btnLoadTestcasePressed();
		}
		if (src == view.getBtnRun()) {
			btnRunPressed();
		}
		if (src == view.getBtnSelectLogDirectory()) {
			btnSelectLogDirectoryPressed();
		}
	}
	
	public void btnLoadTestcasePressed() {
		MyLog.log("Load pressed");
		view.getBtnRun().setEnabled(false);
		if(fileChooserSuite.showOpenDialog(view) == JFileChooser.APPROVE_OPTION) {
			testSuiteFile = fileChooserSuite.getSelectedFile();
			initTestFile(testSuiteFile);
		}
		updateStatusOutput(model.getSuiteStatus());
	}
	
	private void btnRunPressed() {
		MyLog.log("Run pressed");
		updateStatusOutput(SuiteStatus.EXECUTING);
		view.getBtnRun().setEnabled(false);
		try {
			model.executeTestSuite();
		} catch (SpecificationException | DeploymentException | ConfigurationException | TestCaseNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			updateStatusOutput(model.getSuiteStatus());
		}
		try {
			model.outputXmlReport();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(view, "Creating XML Report failed!", "Error", JOptionPane.ERROR_MESSAGE);
		} catch (NullPointerException e) {
			MyLog.log("No XML report output directory selected.");
		}
		view.getBtnRun().setEnabled(true);
	}
	
	private void btnSelectLogDirectoryPressed() {
		MyLog.log("Btn Log Directory pressed");
		if (fileChooserLogDir.showOpenDialog(view) == JFileChooser.APPROVE_OPTION) {
			File outDir = fileChooserLogDir.getSelectedFile();
			model.setXmlOutDirectory(outDir);
		}
	}
	
	private void updateStatusOutput(SuiteStatus status) {
		JTextField statusField = view.getTxtTestStatus();
		switch (status) {
		case EXECUTING:
			statusField.setText("Testing");
			statusField.setBackground(Color.ORANGE);
			break;
		case FAILED:
			statusField.setText("Tests failed");
			statusField.setBackground(Color.RED);
			break;
		case NOT_EXECUTED:
			statusField.setText("Not tested");
			statusField.setBackground(Color.GRAY);
			break;
		case SUCCESS:
			statusField.setText("Tests successful");
			statusField.setBackground(Color.GREEN);
			break;
		default:
			break;
		}
	}
}
