package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import java.awt.Color;

@SuppressWarnings("serial")
public class View extends JFrame {

	private JPanel contentPane;
	private JTextField txtCurrentTestcase;
	private JButton btnLoadTestcase;
	private JButton btnRun;
	private JTextArea textArea;
	private JScrollPane scrollPane;
	private JButton btnSelectLogDirectory;
	private JTextField txtTestStatus;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					View frame = new View();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public View() {
		setTitle("BPELUnit Test Runner");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 731, 504);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		panel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		btnLoadTestcase = new JButton("Load Test Suite");
		panel.add(btnLoadTestcase);
		
		btnSelectLogDirectory = new JButton("Select Log Directory");
		panel.add(btnSelectLogDirectory);
		
		btnRun = new JButton("Run");
		btnRun.setEnabled(false);
		panel.add(btnRun);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.SOUTH);
		panel_1.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		JLabel lblCurrentTestcase = new JLabel("Current Test Suite:");
		panel_1.add(lblCurrentTestcase);
		
		txtCurrentTestcase = new JTextField();
		txtCurrentTestcase.setEditable(false);
		txtCurrentTestcase.setText("None");
		panel_1.add(txtCurrentTestcase);
		txtCurrentTestcase.setColumns(25);
		
		txtTestStatus = new JTextField();
		txtTestStatus.setBackground(Color.GRAY);
		txtTestStatus.setEditable(false);
		txtTestStatus.setText("Nothing tested");
		panel_1.add(txtTestStatus);
		txtTestStatus.setColumns(10);
		
		textArea = new JTextArea();
		textArea.setEditable(false);
		
		scrollPane = new JScrollPane(textArea);
		contentPane.add(scrollPane, BorderLayout.CENTER);
	}

	public JButton getBtnLoadTestcase() {
		return btnLoadTestcase;
	}
	public JButton getBtnRun() {
		return btnRun;
	}
	public JTextField getTxtCurrentTestcase() {
		return txtCurrentTestcase;
	}
	public JTextArea getTextArea() {
		return textArea;
	}
	public JButton getBtnSelectLogDirectory() {
		return btnSelectLogDirectory;
	}
	public JTextField getTxtTestStatus() {
		return txtTestStatus;
	}
}
